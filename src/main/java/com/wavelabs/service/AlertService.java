package com.wavelabs.service;

import java.util.List;

import com.wavelabs.dao.AlertDao;
import com.wavelabs.model.Alert;

public class AlertService {
	private AlertService(){
		
	}

	public static Boolean saveAlert(Alert alert) {
		return AlertDao.saveAlert(alert);
	}

	public static Alert getAlert(int id) {
		return AlertDao.getAlert(id);
	}

	public static List<Alert> getAllAlertsPerReceiver(int id) {
		return AlertDao.getAllAlertsPerReceiver(id);
	}

	public static Alert getAlertAndUpdate(int id, String status) {
		return AlertDao.getAlertAndUpdate(id, status);
	}
}

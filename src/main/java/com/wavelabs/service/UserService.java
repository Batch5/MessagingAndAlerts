package com.wavelabs.service;

import com.wavelabs.dao.UserDao;
import com.wavelabs.model.User;

public class UserService {
	private UserService(){
		
	}
	public static Boolean saveUser(User user) {
		return UserDao.saveUser(user);
	}

	public static User getUser(int id) {
		return UserDao.getUser(id);
	}

	public static Boolean deleteUser(int id) {
		return UserDao.deleteUser(id);
	}

}

package com.wavelabs.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.wavelabs.dao.MessageDao;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.resource.Words;

public class MessageService {
	 static Logger logger = Logger.getLogger(MessageService.class); 
	private MessageService(){
		
	}
	
	public static Message[] getConversations(int id) {
		return MessageDao.getConversations(id);

	}

	public static List<Message> getConversationMessages(int id) {
		return MessageDao.getConversationMessages(id);

	}

	public static MessageThread getMessagesAndUpdate(int id, String isRead) {
		return MessageDao.getMessagesAndUpdate(id, isRead);
	}

	public static Boolean saveMessage(Message message) {
		for (String s : Words.getStr()) {
			if (message.getBody().contains(s)) {
				logger.info("Use appropriate words in the message...");
				return false;
			}
			
		}
		return MessageDao.saveMessage(message);
	}

	public static Boolean deleteUserConversation(int userId, int messageThreadId) {
		return MessageDao.deleteUserConversation(userId, messageThreadId);

	}

	

}

package com.wavelabs.resource;

import org.apache.log4j.Logger;

public class Words {
	static Logger log = Logger.getLogger(Words.class);
	private static final String[] str = { "waste", "donkey", "idiot", "monkey", "pig", "stupid" };

	private Words() {

	}

	public static void main(String[] args) {
		String message = "this is  waste message.";
		for (String s : getStr()) {
			if (message.contains(s)) {
				log.info("Your message contains inappropriate words...Please use proper words....");
				break;
			}
		}

	}

	public static String[] getStr() {
		return str;
	}

}

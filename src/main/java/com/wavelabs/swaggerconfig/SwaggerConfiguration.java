package com.wavelabs.swaggerconfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.BeanConfig;


public class SwaggerConfiguration  extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
@Override
	public void init(ServletConfig config) throws ServletException {
        super.init(config);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("Messaging and Alerts");
        beanConfig.setVersion("1.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/MessageAlertTestCases/care");
        beanConfig.setResourcePackage("com.wavelabs.resource");
        beanConfig.setScan(true);
        beanConfig.setDescription("Message specific operations.");
    }
}

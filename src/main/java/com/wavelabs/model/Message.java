package com.wavelabs.model;

import java.util.Calendar;

public class Message {
	private int id;
	private String subject;
	private String body;
	private boolean isreply;
	private User fromId;
	private User toId;
	private MessageThread messagethread;
	private Calendar timeStamp;

	public Message(String subject, String body, boolean isreply, User fromId, User toId,
			MessageThread messagethread, Calendar timeStamp) {
		super();
		this.subject = subject;
		this.body = body;
		this.isreply = isreply;
		this.fromId = fromId;
		this.toId = toId;
		this.messagethread = messagethread;
		this.timeStamp = timeStamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isIsreply() {
		return isreply;
	}

	public void setIsreply(boolean isreply) {
		this.isreply = isreply;
	}

	public User getFromId() {
		return fromId;
	}

	public void setFromId(User fromId) {
		this.fromId = fromId;
	}

	public User getToId() {
		return toId;
	}

	public void setToId(User toId) {
		this.toId = toId;
	}

	public MessageThread getMessagethread() {
		return messagethread;
	}

	public void setMessagethread(MessageThread messagethread) {
		this.messagethread = messagethread;
	}

	public Calendar getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Calendar timeStamp) {
		this.timeStamp = timeStamp;
	}

}

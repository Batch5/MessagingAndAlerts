package com.wavelabs.model;

public class Alert {
	private int id;
	private User fromId;
	private User toId;
	private String text;
	private AlertMetadata metadata;
	private Status status;

	public Alert(int id, User fromId, User toId, String text, AlertMetadata metadata, Status status) {
		super();
		this.id = id;
		this.fromId = fromId;
		this.toId = toId;
		this.text = text;
		this.metadata = metadata;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getFromId() {
		return fromId;
	}

	public void setFromId(User fromId) {
		this.fromId = fromId;
	}

	public User getToId() {
		return toId;
	}

	public void setToId(User toId) {
		this.toId = toId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public AlertMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(AlertMetadata metadata) {
		this.metadata = metadata;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}

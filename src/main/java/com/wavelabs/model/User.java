package com.wavelabs.model;

public class User {

	private int id;
	private String firstName;
	private String lastName;
	private boolean isactive;

	public User(int id, String firstName, String lastName, boolean isactive) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.isactive = isactive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

}

package com.wavelabs.model;

public enum AlertType {
 ACCEPTDECLINE,
 VIEWONLY,
 VIEWDESTINATION;
}

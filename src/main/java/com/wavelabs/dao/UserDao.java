package com.wavelabs.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.wavelabs.model.User;
import com.wavelabs.utility.Helper;

public class UserDao {
	static Logger logger = Logger.getLogger(UserDao.class);
	static Session session = null;

	private UserDao() {

	}

	public static User getUser(int id) {
		
		session = Helper.getSession();
		User user = (User) session.get(User.class, id);
		session.close();
		return user;
	}

	public static boolean saveUser(User user) {

		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			session.save(user);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();
		}
	}


	public static boolean deleteUser(int id) {
		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			User user = (User) session.get(User.class, id);
			session.delete(user);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();

		}

	}
}

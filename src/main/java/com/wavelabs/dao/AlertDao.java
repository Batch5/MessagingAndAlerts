package com.wavelabs.dao;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.wavelabs.model.Alert;
import com.wavelabs.model.Status;
import com.wavelabs.model.User;
import com.wavelabs.utility.Helper;

public class AlertDao {
	static Logger logger = Logger.getLogger(AlertDao.class);

	private AlertDao() {

	}

	public static Boolean saveAlert(Alert alert) {

		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			session.save(alert);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} finally {
			session.close();
		}
	}

	public static Alert getAlert(int id) {
		Alert alert = null;

		Session session = Helper.getSession();
		try {
			alert = (Alert) session.get(Alert.class, id);
			alert.setStatus(Status.VIEWED);
			session.update(alert);
			session.beginTransaction().commit();

			return alert;
		} catch (Exception e) {
			logger.info(e);
			return alert;
		} finally {
			session.close();
		}
	}

	public static List<Alert> getAllAlertsPerReceiver(int id) {
		Session session = Helper.getSession();
		try {
			session.getTransaction().begin();
			String hql = "FROM Alert a where a.to_id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, session.get(User.class, id));
			@SuppressWarnings("unchecked")
			List<Alert> results = query.list();

			return results;
		} catch (Exception e) {
			logger.info(e);

		} finally {
			session.close();
		}
		return Collections.<Alert>emptyList();

	}

	public static Alert getAlertAndUpdate(int id, String status) {
		String status1 = "accept";
		String status2 = "decline";
		Session session = Helper.getSession();
		try {
			Alert alert = (Alert) session.get(Alert.class, id);
			if (status.equals(status1)) {
				alert.setStatus(Status.ACCEPT);
				session.update(alert);
				session.beginTransaction().commit();
			}
			if (status.equals(status2)) {
				alert.setStatus(Status.DECLINE);
				session.update(alert);
				session.beginTransaction().commit();
			}
			return alert;
		} catch (Exception e) {
			logger.info(e);
		} finally {
			session.close();
		}
		return null;
	}

}

package com.wavelabs.utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.metamodel.Metadata;
import org.hibernate.metamodel.MetadataSources;

public class Helper {
	private static Session session = null;
	private static SessionFactory factory = null;
	private static int count = 0;

	private Helper() {

	}

	public static Session getSession() {
		if (count == 0) {
			setFactory();
		}
		if (!session.isOpen()) {
			session = factory.openSession();
		}
		return session;
	}

	public static SessionFactory getFactory() {
		if (count == 0) {
			setFactory();
		}
		return factory;
	}

	private static void setFactory() {
		StandardServiceRegistry  registry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

	        // Create the MetadataSources
	        MetadataSources sources = new MetadataSources(registry);

	        // Create the Metadata
	        Metadata metadata = sources.getMetadataBuilder().build();

	        // Create SessionFactory
	        factory = metadata.getSessionFactoryBuilder().build();

		session = factory.openSession();
		count++;
	}
}
